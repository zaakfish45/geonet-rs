use colored::Colorize;
use serde::{Deserialize, Serialize};
use serde_json;
use std::collections::BTreeMap;
use structopt::StructOpt;

#[derive(Serialize, Deserialize, Debug)]
#[serde(untagged)]
enum Response {
    Success(DnsResult),
    Error { error: String },
}

#[derive(Serialize, Deserialize, Debug)]
struct DnsResult {
    answers: Vec<DnsAnswer>,
    from_loc: Location,
}

#[derive(Serialize, Deserialize, Debug)]
struct DnsAnswer {
    r#type: String,
    value: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct Location {
    city: String,
    country: String,
    latlon: String,
}

#[derive(Debug, StructOpt)]
#[structopt(
    name = "geodns",
    about = "Lookup a DNS record from different locations aorund the world"
)]
struct Cli {
    /// Output format (shell or json)
    #[structopt(default_value = "shell", short, long)]
    output: String,

    /// Record type to query
    #[structopt(default_value = "A", short = "t", long = "type")]
    rtype: String,

    /// Hostname/ domain to get information for
    fqdn: String,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = Cli::from_args();

    // Send the GeoNet API request and grab the response
    let url = format!(
        "https://geonet.shodan.io/api/geodns/{}?rtype={}",
        args.fqdn, args.rtype
    );
    let results: Vec<Response> = ureq::get(&url.to_string()).call()?.into_json()?;
    let dnsresults: Vec<&DnsResult> = results
        .iter()
        .filter_map(|response| match response {
            Response::Success(result) => Some(result),
            _ => None,
        })
        .collect::<Vec<_>>();

    if dnsresults.is_empty() {
        panic!("Invalid hostname");
    }

    // For JSON output we don't need to do any additional processing
    if args.output == "json" {
        for result in dnsresults.iter() {
            println!("{}", serde_json::to_string(*result)?)
        }
    } else {
        // Group the results by value so it's easier to tell which locations
        // are getting different DNS results.
        let mut dnsmap: BTreeMap<&String, Vec<&Location>> = BTreeMap::new();
        for result in dnsresults.iter() {
            for answer in result.answers.iter() {
                // Initialize the vector for the key if it doesn't yet exist
                if !dnsmap.contains_key(&answer.value) {
                    dnsmap.insert(&answer.value, Vec::new());
                }

                // The key should always exist since we're checking for it right above
                let vec = dnsmap.get_mut(&answer.value).ok_or("Key doesn't exist")?;
                vec.push(&result.from_loc);
            }
        }

        // We got some DNS answers
        if dnsmap.len() > 0 {
            for (answer, locations) in &dnsmap {
                println!(
                    "{:30} {}",
                    answer.bold().white(),
                    &locations[0].city.dimmed()
                );

                if locations.len() > 1 {
                    for location in &locations[1..] {
                        println!("{:30} {}", "".to_string(), location.city.dimmed());
                    }
                    println!("");
                }
            }
        } else {
            println!("No results found");
        }
    }

    Ok(())
}
