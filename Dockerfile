# Prebuilt image for cross-compiling, for now, it supports generating x64 MacOS, Windows executables.
FROM rust:latest

RUN apt-get update --yes && apt-get install clang cmake gcc-mingw-w64-x86-64 --yes

ENV MACOSX_CROSS_COMPILER=/macosx-cross-compiler
RUN mkdir -p $MACOSX_CROSS_COMPILER/cross-compiler

# Clone toolchain build scripts
WORKDIR $MACOSX_CROSS_COMPILER
RUN git clone https://github.com/tpoechtrager/osxcross

# Download prebuilt MacOSX SDK
RUN wget https://github.com/phracker/MacOSX-SDKs/releases/download/11.3/MacOSX11.3.sdk.tar.xz -P osxcross/tarballs

# Build MacOS toolchain
RUN UNATTENDED=yes OSX_VERSION_MIN=10.14.4 TARGET_DIR=$MACOSX_CROSS_COMPILER/cross-compiler ./osxcross/build.sh
ENV PATH=$MACOSX_CROSS_COMPILER/cross-compiler/bin:$PATH
COPY cargo-config.toml /usr/local/cargo/config

# Add x64 targets
RUN rustup target add x86_64-apple-darwin x86_64-pc-windows-gnu
